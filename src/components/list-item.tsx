import React from 'react';
import ListGroupItem from 'react-bootstrap/ListGroupItem';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faTimesCircle, faPencilAlt,
} from '@fortawesome/free-solid-svg-icons';
import Button from 'react-bootstrap/Button';
import { formatMoney } from '../util/util';
import ToolTip from './list-tooltip';

//  Item string formatting functions
const formatQuantity = (quantity: number, units: string) => (quantity && units ? `${quantity} ${units} ` : quantity ? `${quantity} ` : '');
const formatPrice = (cost: number, units: string) => (cost && units ? `${formatMoney(cost)}/${units}` : '$0');
const formatTotalCost = (cost: number, quantity: number) => (cost && quantity ? `${formatMoney(cost * quantity)}` : '$0');

type ListItemProps = {
  name: string;
  onDelete?: any;
  onEdit?: any;
  cost?: number;
  costUnits?: string;
  id?: string;
  density?: number;
  densityUnits?: string;
  quantity?: number;
  onRemove: any;
};
const ListItem = ({
  name, id, density = 0, densityUnits = '', cost = 0, costUnits = '', onRemove, onEdit, quantity = 0,
} : ListItemProps) => {
  const totalCostString = formatTotalCost(cost, quantity);
  const quantityString = formatQuantity(quantity, costUnits);
  const priceString = formatPrice(cost, costUnits);
  const densityString = density ? `${density} ${densityUnits}` : '';

  return (
    <ListGroupItem key={id} className="m-2">
      <Container>
        <Row xs={2} md={3} lg={6}>
          <Col className="p-2">
            {' '}
            <ToolTip text="Cost of material used">
              <h3 id={id} className="item-price">{totalCostString}</h3>
            </ToolTip>
          </Col>
          <Col className="p-2">
            <ToolTip text="Type of material">
              <h3 id={id}>{name}</h3>
            </ToolTip>
          </Col>
          <Col className="p-2">
            <ToolTip text="Amount of material">
              <h3 id={id} className="item-quantity align-middle">{quantityString}</h3>
            </ToolTip>
          </Col>
          <Col className="p-2">
            <ToolTip text="Cost">
              <h3 id={id} className="item-price ">{priceString}</h3>
            </ToolTip>
          </Col>
          <Col className="p-2">
            <ToolTip text="Density">
              <h3 id={id} className="item-density ">{densityString}</h3>
            </ToolTip>
          </Col>
          <Col className="py-2 px-3 ">
            <ButtonGroup aria-label="edit close">
              <ToolTip text="Edit Item">
                <Button variant="outline-dark" title="Edit item" onClick={onEdit}>
                  <FontAwesomeIcon icon={faPencilAlt} size="lg" />
                </Button>
              </ToolTip>
              <ToolTip text="Delete Item">
                <Button variant="outline-dark" title="Delete item" onClick={onRemove}>
                  <FontAwesomeIcon icon={faTimesCircle} size="lg" />
                </Button>
              </ToolTip>
            </ButtonGroup>

          </Col>
        </Row>
      </Container>

    </ListGroupItem>
  );
};

export default ListItem;

import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { formatMoney } from '../util/util';

type ListFooterProps = {
  cost: number;
};

const ListFooter = ({ cost }:ListFooterProps) => (
  <Container fluid="md" className="list-footer">
    <Row>
      <Col className="p-2">
        <h2>
          Total Cost
          {' '}
          {formatMoney(cost)}
        </h2>
      </Col>
    </Row>
  </Container>
);

export default ListFooter;

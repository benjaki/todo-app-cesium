import React from 'react';
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { useForm } from 'react-hook-form';

type ListFormProps = {
  name?: string;
  cost?: number;
  costUnits?: string;
  id?: string;
  density?: number;
  densityUnits?: string;
  quantity?: number;
  onSubmit: any;
  onClose: any;
};

const ListForm = (props: ListFormProps) => {
  const {
    name, id, density, onClose, densityUnits, cost, costUnits, quantity, onSubmit,
  } = props;
  const { register, handleSubmit } = useForm();

  // some of these form groups
  // could be broken down into
  // smaller components
  return (
    <Container className="p-2">
      <Row>
        <Col md={{ span: 8, offset: 2 }}>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Container>
              <Row className="p-1">
                <Col>
                  <Form.Group controlId="material-name">
                    <Form.Label>
                      Name
                      <i>- required</i>
                    </Form.Label>
                    <Form.Control name="name" ref={register} required type="text" placeholder="Name" defaultValue={name} />
                    <Form.Text>Name of material type</Form.Text>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group controlId="material-id">
                    <Form.Label>ID</Form.Label>
                    <Form.Control name="id" ref={register} type="text" placeholder="ID" defaultValue={id} />
                    <Form.Text>ID of material type</Form.Text>
                  </Form.Group>
                </Col>
              </Row>
              <Row className="p-1">
                <Col>
                  <Form.Group controlId="material-density">
                    <Form.Label>Density</Form.Label>
                    <Form.Control name="density" ref={register} step="any" type="number" placeholder="Density" defaultValue={density} />
                    <Form.Text>Numeric Density of Material</Form.Text>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group controlId="material-density-units">
                    <Form.Label>Units of Density</Form.Label>
                    <Form.Control name="densityUnits" type="text" ref={register} as="select" defaultValue={densityUnits}>
                      <option>mg/mL</option>
                      <option>g/mL</option>
                      <option>g/L</option>
                      <option>g/cm^3</option>
                      <option>kg/L</option>
                    </Form.Control>

                  </Form.Group>
                </Col>
              </Row>
              <Row className="p-1">
                <Col>
                  <Form.Group controlId="material-cost">
                    <Form.Label>Cost($)</Form.Label>
                    <Form.Control name="cost" ref={register} step="any" type="number" placeholder="Cost" defaultValue={cost} />
                    <Form.Text>Numeric cost </Form.Text>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group controlId="material-cost-units">
                    <Form.Label>Unit of Weight</Form.Label>
                    <Form.Control name="costUnits" ref={register} type="text" as="select" defaultValue={costUnits}>
                      <option>g</option>
                      <option>kg</option>
                      <option>mg</option>
                      <option>lb</option>
                      <option>oz</option>
                      <option>t</option>
                    </Form.Control>
                  </Form.Group>
                </Col>
              </Row>
              <Row className="p-1">
                <Col>
                  <Form.Group controlId="material-quantity">
                    <Form.Label>Quantity</Form.Label>
                    <Form.Control name="quantity" ref={register} type="number" placeholder="Quantity" defaultValue={quantity} />
                    <Form.Text>Amount of material in terms of weight unit selected</Form.Text>
                  </Form.Group>
                </Col>
              </Row>
            </Container>
            <Container>
              <Row className="p-1">
                <Col>
                  <Button variant="outline-primary" className="btn" type="submit">Submit</Button>
                  {' '}
                  <Button variant="outline-secondary" onClick={onClose}>Cancel</Button>
                </Col>
              </Row>
            </Container>
          </Form>
        </Col>
      </Row>
    </Container>

  );
};
export default ListForm;

import React from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

interface TooltipProps {
  text: string
}

const ToolTip = ({ children, text }: React.PropsWithChildren<TooltipProps>) => (
  <>
    <OverlayTrigger
      placement="top-start"
      overlay={(
        <Tooltip id="tooltip">
          {text}
        </Tooltip>
    )}
    >
      {children}
    </OverlayTrigger>
    {' '}
  </>
);
export default ToolTip;

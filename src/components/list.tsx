import React, { useState } from 'react';
import {
  ListGroup, Container, Button,
} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import ListItem from './list-item';
import ListFooter from './list-footer';
import ListForm from './list-form';
import {
  Item, UpdateMaterial, RemoveMaterial, AddMaterial,
} from '../types/materials';

const defaultState = { hasFormOpen: false, updateIndex: -1, isUpdate: false };

type ListProps = {
  title: string;
  height?: number;
  addMaterial: (item: Item) => AddMaterial;
  removeMaterial: (i: number) => RemoveMaterial;
  updateMaterial: (i: number, item: Item) => UpdateMaterial;
  items: Item[];
};
const List = ({
  items, title, addMaterial, removeMaterial, updateMaterial, height,
}: ListProps) => {
  const [materialState, updateMaterialState] = useState(defaultState);
  const { hasFormOpen, updateIndex, isUpdate } = materialState;
  const updatedItem = isUpdate ? items[updateIndex] : {};
  let totalCost = 0;
  return (
    <Container className="material-container">
      <div className="d-flex justify-content-between bg-light p-2">
        <h1 className="list-header ">{isUpdate ? 'Edit Material' : hasFormOpen ? 'Add Material' : title }</h1>
        {!hasFormOpen ? (
          <Button variant="outline-dark" onClick={() => updateMaterialState({ hasFormOpen: true, updateIndex: -1, isUpdate: false })}>
            <h4>
              <FontAwesomeIcon icon={faPlusCircle} />
              {' '}
              {' '}
              Add Item
            </h4>
          </Button>
        ) : null }
      </div>
      <div style={height ? { height: `${height}px` } : {}} className={height ? 'overflow-auto' : ''}>
        {!hasFormOpen ? (
          <>
            <ListGroup>
              {items.map((item, i) => {
                const { cost, quantity } = item;
                // Use loop to calculate total cost
                totalCost += cost && quantity ? cost * quantity : 0;
                return (
                  <ListItem
                    key={`${item.name}-${item.id}`}
                    {...item}
                    onRemove={() => removeMaterial(i)}
                    onEdit={() => {
                      updateMaterialState({ hasFormOpen: true, updateIndex: i, isUpdate: true });
                    }}
                  />
                );
              })}
            </ListGroup>

            <ListFooter cost={totalCost} />
          </>
        ) : (
          <ListForm
            {...updatedItem}
            onSubmit={(data: any) => {
              updateMaterialState(defaultState);

              const itemData: Item = {
                name: data.name,
                id: data.id,
                densityUnits: data.densityUnits,
                density: parseFloat(data.density || '') || 0,
                quantity: parseFloat(data.quantity || '') || 0,
                cost: parseFloat(data.cost || '') || 0,
                costUnits: data.costUnits,
              };
              return !isUpdate ? addMaterial(itemData) : updateMaterial(updateIndex, itemData);
            }}
            onClose={() => {
              updateMaterialState(defaultState);
            }}
          />
        )}
      </div>
    </Container>
  );
};

export default List;

import React from 'react';
import { render } from '@testing-library/react';
import List from './list';
import { removeMaterial, updateMaterial, addMaterial } from '../actions/materials';
import { Item } from '../types/materials';

const mockProps = {
  title: 'Materials',
  addMaterial: (item:Item) => addMaterial(item),
  removeMaterial: (i: number) => removeMaterial(i),
  updateMaterial: (i: number, item:Item) => updateMaterial(i, item),
  items: [{ name: 'Wood' }],
};
test('Header renders', () => {
  const { getByText } = render(<List {...mockProps} />);
  const linkElement = getByText(/Materials/i);
  expect(linkElement).toBeInTheDocument();
});
test('Item name renders', () => {
  const { getByText } = render(<List {...mockProps} />);
  const linkElement = getByText(/Wood/i);
  expect(linkElement).toBeInTheDocument();
});

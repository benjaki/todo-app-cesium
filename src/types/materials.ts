export interface Item {
  name: string;
  cost?: number;
  costUnits?: string;
  id?: string;
  density?: number;
  densityUnits?: string;
  quantity?: number;
}

export enum ActionTypes {
  ADD_MATERIAL = 'ADD_MATERIAL',
  UPDATE_MATERIAL = 'UPDATE_MATERIAL',
  REMOVE_MATERIAL = 'REMOVE_MATERIAL',
}

export interface AddMaterial {
  type: ActionTypes;
  item: Item;
}

export interface UpdateMaterial {
  type: ActionTypes;
  index: number;
  item: Item;
}

export interface RemoveMaterial {
  type: ActionTypes;
  index: number;
}
export type MaterialActionTypes = UpdateMaterial | RemoveMaterial | AddMaterial;

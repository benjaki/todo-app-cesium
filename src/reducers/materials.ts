import {
  ActionTypes, MaterialActionTypes, Item, UpdateMaterial, RemoveMaterial, AddMaterial,
} from '../types/materials';

const defaultState: State = {
  items: [],
  title: 'Materials',
};
type State = {
  readonly title: string;
  readonly items: Item[];
};
const materialsReducer = (state = defaultState, action: MaterialActionTypes) => {
  switch (action.type) {
    case ActionTypes.ADD_MATERIAL:
      return { ...state, items: [...state.items, (action as AddMaterial).item] };
    case ActionTypes.UPDATE_MATERIAL:
      return {
        ...state,
        items: state.items.map((item, index) => {
          if (index !== (action as UpdateMaterial).index) {
            return item;
          }
          return {
            ...(action as UpdateMaterial).item,
          };
        }),
      };
    case ActionTypes.REMOVE_MATERIAL:
      return {
        ...state,
        items: [
          ...state.items.slice(0, (action as RemoveMaterial).index),
          ...state.items.slice((action as RemoveMaterial).index + 1),
        ],
      };

    default:
      return state;
  }
};

export default materialsReducer;

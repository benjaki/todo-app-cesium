import { combineReducers } from 'redux';
import materialsReducer from './materials';

const rootReducer = combineReducers({
  materials: materialsReducer,
});

export default rootReducer;

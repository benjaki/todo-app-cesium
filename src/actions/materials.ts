import {
  Item, ActionTypes,
} from '../types/materials';


export function addMaterial(item: Item) {
  return {
    type: ActionTypes.ADD_MATERIAL,
    item,
  };
}

export function updateMaterial(index: number, item: Item) {
  return {
    type: ActionTypes.UPDATE_MATERIAL,
    index,
    item,
  };
}
export function removeMaterial(index: number) {
  return {
    type: ActionTypes.REMOVE_MATERIAL,
    index,
  };
}

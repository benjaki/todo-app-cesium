// this function can be expanded to include commas or replaced by lib
// eslint-disable-next-line import/prefer-default-export
export function formatMoney(n: number): string {
  return `$${Number((n).toFixed(2))}`;
}

import { formatMoney } from './util';

test('Header renders', () => {
  expect(formatMoney(123123.3213)).toBe('$123123.32');
});

import React from 'react';
import { Container } from 'react-bootstrap';
import { connect, ConnectedProps } from 'react-redux';
import MaterialList from './components/list';
import 'bootstrap/dist/css/bootstrap.min.css';
import { addMaterial, removeMaterial, updateMaterial } from './actions/materials';
import { Item } from './types/materials';

interface AppState {
  materials: {
    items: Item[];
    title: string;
  };
}
const mapState = (state: AppState) => ({
  items: state.materials.items,
  title: state.materials.title,
});
const mapDispatch = {
  addMaterial: (item: Item) => (addMaterial(item)),
  removeMaterial: (index: number) => (removeMaterial(index)),
  updateMaterial: (index: number, item: Item) => (updateMaterial(index, item)),
};
const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;


function App(props: PropsFromRedux) {
  return (
    <div className="App">
      <Container>
        <MaterialList {...props} />
      </Container>
    </div>
  );
}


export default connector(App);
